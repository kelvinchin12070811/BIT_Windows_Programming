﻿namespace Program3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.listLeft = new System.Windows.Forms.ListBox();
            this.listRight = new System.Windows.Forms.ListBox();
            this.btnLToR = new System.Windows.Forms.Button();
            this.btnLToRAll = new System.Windows.Forms.Button();
            this.btnRToL = new System.Windows.Forms.Button();
            this.btnRToLAll = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(167, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(445, 37);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select Item From The Boxes";
            // 
            // listLeft
            // 
            this.listLeft.FormattingEnabled = true;
            this.listLeft.ItemHeight = 20;
            this.listLeft.Location = new System.Drawing.Point(33, 99);
            this.listLeft.Name = "listLeft";
            this.listLeft.Size = new System.Drawing.Size(275, 304);
            this.listLeft.Sorted = true;
            this.listLeft.TabIndex = 1;
            // 
            // listRight
            // 
            this.listRight.FormattingEnabled = true;
            this.listRight.ItemHeight = 20;
            this.listRight.Location = new System.Drawing.Point(495, 99);
            this.listRight.Name = "listRight";
            this.listRight.Size = new System.Drawing.Size(275, 304);
            this.listRight.Sorted = true;
            this.listRight.TabIndex = 2;
            // 
            // btnLToR
            // 
            this.btnLToR.Location = new System.Drawing.Point(352, 130);
            this.btnLToR.Name = "btnLToR";
            this.btnLToR.Size = new System.Drawing.Size(103, 53);
            this.btnLToR.TabIndex = 3;
            this.btnLToR.Text = ">";
            this.btnLToR.UseVisualStyleBackColor = true;
            this.btnLToR.Click += new System.EventHandler(this.BtnLToR_Click);
            // 
            // btnLToRAll
            // 
            this.btnLToRAll.Location = new System.Drawing.Point(352, 189);
            this.btnLToRAll.Name = "btnLToRAll";
            this.btnLToRAll.Size = new System.Drawing.Size(103, 53);
            this.btnLToRAll.TabIndex = 4;
            this.btnLToRAll.Text = ">>";
            this.btnLToRAll.UseVisualStyleBackColor = true;
            this.btnLToRAll.Click += new System.EventHandler(this.BtnLToRAll_Click);
            // 
            // btnRToL
            // 
            this.btnRToL.Location = new System.Drawing.Point(352, 248);
            this.btnRToL.Name = "btnRToL";
            this.btnRToL.Size = new System.Drawing.Size(103, 53);
            this.btnRToL.TabIndex = 5;
            this.btnRToL.Text = "<";
            this.btnRToL.UseVisualStyleBackColor = true;
            this.btnRToL.Click += new System.EventHandler(this.BtnRToL_Click);
            // 
            // btnRToLAll
            // 
            this.btnRToLAll.Location = new System.Drawing.Point(352, 307);
            this.btnRToLAll.Name = "btnRToLAll";
            this.btnRToLAll.Size = new System.Drawing.Size(103, 53);
            this.btnRToLAll.TabIndex = 6;
            this.btnRToLAll.Text = "<<";
            this.btnRToLAll.UseVisualStyleBackColor = true;
            this.btnRToLAll.Click += new System.EventHandler(this.BtnRToLAll_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnRToLAll);
            this.Controls.Add(this.btnRToL);
            this.Controls.Add(this.btnLToRAll);
            this.Controls.Add(this.btnLToR);
            this.Controls.Add(this.listRight);
            this.Controls.Add(this.listLeft);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listLeft;
        private System.Windows.Forms.ListBox listRight;
        private System.Windows.Forms.Button btnLToR;
        private System.Windows.Forms.Button btnLToRAll;
        private System.Windows.Forms.Button btnRToL;
        private System.Windows.Forms.Button btnRToLAll;
    }
}

