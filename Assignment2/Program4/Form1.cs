﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Program4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void applyFont()
        {
            float szFont = 8.0f;
            FontStyle style = FontStyle.Regular;

            if (sz12.Checked)
                szFont = 12.0f;
            else if (sz20.Checked)
                szFont = 20.0f;
            else if (sz30.Checked)
                szFont = 30.0f;

            if (ckbBold.Checked)
                style |= FontStyle.Bold;

            if (ckbItalic.Checked)
                style |= FontStyle.Italic;

            if (ckbUnderline.Checked)
                style |= FontStyle.Underline;

            input.Font = new Font(input.Font.FontFamily, szFont, style);

            if (colBlue.Checked)
                input.ForeColor = Color.Blue;
            else if (colGreen.Checked)
                input.ForeColor = Color.Green;
            else if (colRed.Checked)
                input.ForeColor = Color.Red;
            else
                input.ForeColor = Color.Black;
        }

        private void CkbBold_CheckedChanged(object sender, EventArgs e)
        {
            applyFont();
        }

        private void CkbItalic_CheckedChanged(object sender, EventArgs e)
        {
            applyFont();
        }

        private void CkbUnderline_CheckedChanged(object sender, EventArgs e)
        {
            applyFont();
        }

        private void Sz12_CheckedChanged(object sender, EventArgs e)
        {
            applyFont();
        }

        private void Sz20_CheckedChanged(object sender, EventArgs e)
        {
            applyFont();
        }

        private void Sz30_CheckedChanged(object sender, EventArgs e)
        {
            applyFont();
        }

        private void ColRed_CheckedChanged(object sender, EventArgs e)
        {
            applyFont();
        }

        private void ColGreen_CheckedChanged(object sender, EventArgs e)
        {
            applyFont();
        }

        private void ColBlue_CheckedChanged(object sender, EventArgs e)
        {
            applyFont();
        }
    }
}
