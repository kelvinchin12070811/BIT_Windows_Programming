﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Program2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            itmName.ResetText();
            itmCount.ResetText();
            itmPrice.ResetText();
            totAmount.ResetText();
            totDiscount.ResetText();
            totAfterDiscount.ResetText();
        }

        private void BtnCalculate_Click(object sender, EventArgs e)
        {
            int itmCount = 0;
            double itmPrice = 0;
            double total = 0;
            double discount = 0;

            if (!int.TryParse(this.itmCount.Text, out itmCount))
            {
                MessageBox.Show(string.Format("failed to convert \"{0}\" to int", this.itmCount.Text));
                return;
            }

            if (!double.TryParse(this.itmPrice.Text, out itmPrice))
            {
                MessageBox.Show(string.Format("failed to convert \"{0}\" to double", this.itmPrice.Text));
                return;
            }
            

            total = itmCount * itmPrice;
            discount = total * .3;

            totAmount.Text = string.Format("{0:0.00}", total);
            totDiscount.Text = string.Format("{0:0.00}", discount);
            totAfterDiscount.Text = string.Format("{0:0.00}", total * .7);
        }
    }
}
