﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Program1
{
    class Database
    {
        private static bool unsave = false;
        private static SortedDictionary<string, string> usrAccs = new SortedDictionary<string, string>();
        
        public static void AddUser(string usrName, string usrPass)
        {
            usrAccs.Add(usrName, usrPass);
            unsave = true;
        }

        public static bool ContainUser(string usrName)
        {
            return usrAccs.ContainsKey(usrName);
        }

        public static void DumpAllDb()
        {
            if (!unsave) return;

            StreamWriter dbWriter = new StreamWriter("accounts.db.txt");
            foreach (KeyValuePair<string, string> itr in usrAccs)
            {
                dbWriter.WriteLine(itr.Key);
                dbWriter.WriteLine(itr.Value);
            }
            dbWriter.Close();
            unsave = false;
        }

        public static void InitAllDb()
        {
            StreamReader dbReader = new StreamReader("accounts.db.txt");

            string line = null;
            while ((line = dbReader.ReadLine()) != null)
            {
                string usrName = line;
                string usrPass = dbReader.ReadLine();

                usrAccs.Add(usrName, usrPass);
            }
            dbReader.Close();
        }

        public static bool VerifyUser(string usrName, string usrPassword)
        {
            string rsltPassword = null;
            if (!usrAccs.TryGetValue(usrName, out rsltPassword))
                return false;

            if (usrPassword != rsltPassword)
                return false;

            return true;
        }
    }
}
