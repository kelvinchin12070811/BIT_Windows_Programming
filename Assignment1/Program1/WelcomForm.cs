﻿using System.Drawing;
using System.Windows.Forms;

namespace Program1
{
    public partial class WelcomForm : Form
    {
        private Form parent = null;

        public WelcomForm()
        {
            InitializeComponent();
            imgUsrAvatar.Image = Image.FromFile("images/profile.jpg");
        }

        public void Show(Form parent, string usrname)
        {
            lbUsrname.Text = usrname;
            this.parent = parent;
            this.parent.Hide();
            this.Show();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            if (parent != null) parent.Close();
        }
    }
}
