﻿using System.Drawing;
using System.Windows.Forms;

namespace Program1
{
    public partial class WrongForm : Form
    {
        private Form parent = null;

        public WrongForm()
        {
            InitializeComponent();
            imgWrong.Image = Image.FromFile("images/wrong_page.png");
        }

        public void Show(Form parent)
        {
            this.parent = parent;
            this.parent.Hide();
            this.Show();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            parent.Show();
        }
    }
}
