﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Program1
{
    public partial class RegisterForm : Form
    {
        private Form parent = null;
        public RegisterForm()
        {
            InitializeComponent();

            forumLogo.Image = Image.FromFile("images/forum_logo.png");
        }

        public void Show(Form parent)
        {
            this.parent = parent;
            this.parent.Hide();
            this.Show();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (parent != null)
                parent.Show();

            base.OnFormClosing(e);
        }

        private void VerifyCredential()
        {
            if (Database.ContainUser(inputUsername.Text))
            {
                MessageBox.Show("Username " + inputUsername.Text + " has been taken");
                return;
            }

            Database.AddUser(inputUsername.Text, inputPassword.Text);
        }

        private void BtnRegister_Click(object sender, EventArgs e)
        {
            if (inputUsername.Text == "")
            {
                MessageBox.Show("Please input a valid user name");
                return;
            }

            if (inputPassword.Text.Length < 8)
            {
                MessageBox.Show("Password must be at least 8 letters long.");
                return;
            }

            if (inputConfirmPass.Text != inputPassword.Text)
            {
                MessageBox.Show("Both password entered are not equal.");
                return;
            }

            VerifyCredential();
            MessageBox.Show("Register successfully");
            this.Close();
        }
    }
}
