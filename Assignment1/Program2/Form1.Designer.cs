﻿namespace Program2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.shapeViewer = new System.Windows.Forms.PictureBox();
            this.rbCircle = new System.Windows.Forms.RadioButton();
            this.rbOval = new System.Windows.Forms.RadioButton();
            this.rbSquare = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.shapeViewer)).BeginInit();
            this.SuspendLayout();
            // 
            // shapeViewer
            // 
            this.shapeViewer.Location = new System.Drawing.Point(12, 12);
            this.shapeViewer.Name = "shapeViewer";
            this.shapeViewer.Size = new System.Drawing.Size(200, 200);
            this.shapeViewer.TabIndex = 0;
            this.shapeViewer.TabStop = false;
            // 
            // rbCircle
            // 
            this.rbCircle.AutoSize = true;
            this.rbCircle.Location = new System.Drawing.Point(232, 49);
            this.rbCircle.Name = "rbCircle";
            this.rbCircle.Size = new System.Drawing.Size(51, 17);
            this.rbCircle.TabIndex = 1;
            this.rbCircle.Text = "Circle";
            this.rbCircle.UseVisualStyleBackColor = true;
            this.rbCircle.CheckedChanged += new System.EventHandler(this.RbCircle_CheckedChanged);
            // 
            // rbOval
            // 
            this.rbOval.AutoSize = true;
            this.rbOval.Location = new System.Drawing.Point(232, 99);
            this.rbOval.Name = "rbOval";
            this.rbOval.Size = new System.Drawing.Size(47, 17);
            this.rbOval.TabIndex = 2;
            this.rbOval.Text = "Oval";
            this.rbOval.UseVisualStyleBackColor = true;
            this.rbOval.CheckedChanged += new System.EventHandler(this.RbOval_CheckedChanged);
            // 
            // rbSquare
            // 
            this.rbSquare.AutoSize = true;
            this.rbSquare.Location = new System.Drawing.Point(232, 144);
            this.rbSquare.Name = "rbSquare";
            this.rbSquare.Size = new System.Drawing.Size(59, 17);
            this.rbSquare.TabIndex = 3;
            this.rbSquare.Text = "Square";
            this.rbSquare.UseVisualStyleBackColor = true;
            this.rbSquare.CheckedChanged += new System.EventHandler(this.RbSquare_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 223);
            this.Controls.Add(this.rbSquare);
            this.Controls.Add(this.rbOval);
            this.Controls.Add(this.rbCircle);
            this.Controls.Add(this.shapeViewer);
            this.Name = "Form1";
            this.Text = "Shape viewer";
            ((System.ComponentModel.ISupportInitialize)(this.shapeViewer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox shapeViewer;
        private System.Windows.Forms.RadioButton rbCircle;
        private System.Windows.Forms.RadioButton rbOval;
        private System.Windows.Forms.RadioButton rbSquare;
    }
}

