﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            var items = comboBox1.Items;
            items.Add("apple 1.0");
            items.Add("apple 2.0");
            items.Add("apple 3.0");
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            string getText = comboBox1.SelectedItem.ToString();
            MessageBox.Show(getText);
        }
    }
}
