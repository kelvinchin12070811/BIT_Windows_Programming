﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        private Form2 f2 = null;
        private Form3 f3 = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (f2 == null)
            {
                f2 = new Form2();
                f2.MdiParent = this;
                f2.FormClosed += new FormClosedEventHandler(F2_FormClosed);
                f2.Show();
            }
            else f2.Activate();
        }

        private void F2_FormClosed(object sender, FormClosedEventArgs e)
        {
            f2 = null;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void Form3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (f3 == null)
            {
                f3 = new Form3
                {
                    MdiParent = this
                };
                f3.FormClosed += new FormClosedEventHandler((object s, FormClosedEventArgs parms) =>
                {
                    f3 = null;
                });
                f3.Show();
            }
            else
                f3.Activate();
        }
    }
}
